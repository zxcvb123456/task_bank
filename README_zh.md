# task_bank
Go1.21 + gin

## 介绍
本仓库是一个**任务银行**程序的后端部分。
该程序借助游戏金币积累的思路打造了一个todo list和生活账单功能相关联的应用。

```shell
# 设置国内源
go env -w GOPROXY=https://goproxy.cn,direct
```
初始化项目

```shell
# 初始化
go mod init task_bank
# 依赖检查
go mod tidy
```

运行
```shell
go run main.go
```

