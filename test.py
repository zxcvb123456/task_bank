import requests

def get(p):
    url = "http://localhost:8080/" + p
    print(requests.get(url).text)

def post(p):
    url = "http://localhost:8080/" + p
    print(requests.post(url).text)

def delete(p):
    url = "http://localhost:8080/" + p
    print(requests.delete(url).text)

def put(p):
    url = "http://localhost:8080/" + p
    print(requests.put(url).text)

if __name__ == "__main__":

    print('get("tasks")')
    get("tasks")
    print('get("tasks/3")')
    get("tasks/3")
    print('post("tasks")')
    post("tasks")
    print('delete("tasks/3")')
    delete("tasks/3")
    print('put("tasks/3")')
    put("tasks/3")
