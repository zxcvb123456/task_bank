package main

import (
	"fmt"
	"log"
	"net/http"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
	"strconv"
)

func main() {

	router := gin.Default()
	router.GET("/cover", getCover)
	router.POST("/user", createUser)
	router.POST("/task/tag", createTaskTag)

	err := router.Run(":8080")
	if err != nil {
		log.Fatal(err)
	}
}

func getCover(c *gin.Context) {
	url := "https://imgapi.cn/api.php?zd=mobile&fl=fengjing"
	c.JSON(
		http.StatusOK, 
		gin.H{
			"text": "非知之难，行之为难！",
			"img_url": url,
		},
	)
}

func createTaskTag(c *gin.Context) {
	// TODO-1: 这里写了URL和form表单两种解析，为了方便前期调试，后面应该统一成form表单形式
	Query := c.Query
	PostForm := c.PostForm

	quid := Query("uid")
	puid := PostForm("uid")

	var (
		userid	uint
		name	string
		color	string
		err	error
	)

	if quid != "" {
		usid, err := strconv.ParseUint(quid, 10, 32)
		userid = uint(usid)
		name = Query("tagName")
		color = Query("tagColor")
		if err != nil {
			c.JSON(
				400, // TODO, 找合适的状态码
				gin.H{},
				)
		}
	} else {
		usid, err := strconv.ParseUint(puid, 10, 32)
		userid = uint(usid)
		name = PostForm("tagName")
		color = PostForm("tagColor")
		if err != nil {
			c.JSON(
				400, // TODO, 找合适的状态码
				gin.H{},
				)
		}
	}

	tasktag := TaskTag{
		Name: name,
		Color: color,
		UserID: userid,
	}

	err = TaskTagAdd(tasktag)
	if err != nil {
		c.JSON(
			400, // TODO, 找合适的状态码
			gin.H{},
			)
	} else {
		c.JSON(
			200, // TODO, 找合适的状态码
			gin.H{},
		)
	}
}

func createUser(c *gin.Context) {
	// TODO: 这里写了URL和form表单两种解析，为了方便前期调试，后面应该统一成form表单形式
	// TODO: 应该增加password加解密功能
	Query := c.Query
	PostForm := c.PostForm

	var uName string
	var uPassword string

	if Query("uName") != "" {
		uName = Query("uName")
		uPassword = Query("uPassword")
	} else {
		uName = PostForm("uName")
		uPassword = PostForm("uPassword")
	}

	user := User{
		Username : uName,
		Password : uPassword,
	}

	fmt.Println(c)
	fmt.Println(user)

	new_user, err := UserAddOrLogin(user)
	if err == nil {  // 成功找到user或添加新user
		c.JSON(
			http.StatusOK,
			gin.H{
				"userdata": new_user,
				"status": 200,
			})
	} else if err == gorm.ErrInvalidValue {
		c.JSON(
			http.StatusUnauthorized, // 401
			gin.H{
				"errData": "Password Incorrect",
				"status": http.StatusUnauthorized,
			})
	} else {
		c.JSON(
			http.StatusServiceUnavailable, // 503
			gin.H{
				"userdata": user,
				"status": http.StatusServiceUnavailable,
			})
	}
}

