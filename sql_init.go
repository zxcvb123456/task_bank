package main

import (
	"fmt"
	"time"
	"github.com/google/uuid"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

var DATABASE = "taskBank.db"

type User struct {
	ID          uint   `gorm:"primaryKey"`
	UID         string `gorm:"unique;not null"`
	Username    string
	Password    string
	Email       string
	Phone       string
	CreateTime  time.Time	`gorm:"autoCreateTime"`
	UpdateTime  time.Time	`gorm:"autoUpdateTime"`
	Avatar      string
}

type TaskTag struct {
	ID          uint   `gorm:"primaryKey"`
	Name	    string
	Color	    string
	UserID      uint
	User	    User `gorm:"foreignKey:UserID"`
}

func initUserTable() error {
	db, err := gorm.Open(sqlite.Open(DATABASE), &gorm.Config{})
	if err != nil {
		return err
	}

	// 自动迁移数据库，创建User表
	err = db.AutoMigrate(&User{})
	if err != nil {
		return err
	}

	// 检查User表是否存在
	if !db.Migrator().HasTable(&User{}) {
		return fmt.Errorf("failed to create User table")
	}

	return nil
}

func initTaskTagTable() error {
	db, err := gorm.Open(sqlite.Open(DATABASE), &gorm.Config{})
	if err != nil {
		return err
	}

	err = db.AutoMigrate(&TaskTag{})
	if err != nil {
		return err
	}

	if !db.Migrator().HasTable(&TaskTag{}) {
		return fmt.Errorf("failed to create TaskTag table")
	}

	return nil
}

func UserAddOrLogin(user User) (User, error) {
	db, err := gorm.Open(sqlite.Open(DATABASE), &gorm.Config{})
	if err != nil {
		return user, err
	}

	var db_user User

	UID := uuid.New().String()

	username := user.Username
	dt := db.Where("Username=?", username).Find(&db_user)
	if dt.Error != nil {
		return user, dt.Error
	} else if dt.RowsAffected == 1 { // 找到该用户
		if user.Password == db_user.Password { // 验证密码
			return db_user, nil
		} else {
			return user, gorm.ErrInvalidValue
		}
	} else { // 未找到该用户，新建
		user.UID = UID
		err = db.Create(&user).Error
		if err != nil {
			return user, err
		}
		fmt.Println("User added successfully.")
		var new_user User
		db.Where("Username=?", username).First(&new_user)
		return new_user, nil
	}
}

func TaskTagAdd(tasktag TaskTag) error {
	db, err := gorm.Open(sqlite.Open(DATABASE), &gorm.Config{})
	if err != nil {
		return err
	}

	var db_tasktag TaskTag

	tagname := tasktag.Name
	userid := tasktag.UserID
	color := tasktag.Color
	dt := db.Where("Name=? and User_ID=? and Color=?", tagname, userid, color).Find(&db_tasktag)
	if dt.Error != nil {
		return dt.Error
	} else if dt.RowsAffected == 1 { // 该用户已有该tag
		return gorm.ErrInvalidValue // TODO 修改错误类型
	} else {
		err = db.Create(&tasktag).Error
		if err != nil {
			return err
		}
		fmt.Println("TaskTag added successfully.")
		return nil
	}
}

// func main() {
// 	// initUserTable()
// 	initTaskTagTable()
// }
