# task_bank
Go1.21 + gin

## Intro
The repository is the backend part of a Task Bank program.
The program leverages the concept of accumulating game coins and combines it with a todo list and a life bill functionality.

## Project Init

```shell
# init 
go mod init task_bank
# Check dependency
go mod tidy
```

## run
```shell
go run main.go
```

